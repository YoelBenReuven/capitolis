const transactions = [
    { tradingparty: "Joel", counterParty: "Moshe", amount: 500, counterID: 555 },
    { tradingparty: "Joel", counterParty: "Ohad", amount: 300, counterID: 222 },
    { tradingparty: "Joel", counterParty: "Ohad", amount: -200, counterID: 222 },
    { tradingparty: "Joel", counterParty: "Roi", amount: -50, counterID: 111 }
]
module.exports =transactions;