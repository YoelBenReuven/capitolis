const express = require('express');
const cors = require('cors')


const router = require('./routes/appRoutes');

const app = express();
app.use(cors());
app.use(express.urlencoded({
  extended: true
}));
app.use(express.json());






app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization,multipart/form-data,'
    );
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
  
    next();
  });

app.use(router);







app.listen(5000);