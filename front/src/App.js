
import './App.css';
import React from 'react';
import {Redirect, Route} from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom';
import Transactions from './Pages/transactions';

function App() {

  
  return (
    <Router>
      <Route path="/" exact>
      <Redirect to="/transactions" />
      </Route>
      <Route path="/transactions">
        <Transactions></Transactions>
      </Route>
    </Router>
  );
}

export default App;
