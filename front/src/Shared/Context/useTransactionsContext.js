import React from "react";

const TransactionsContext = React.createContext(undefined);

function TransactionsProvider({ children, value }) { 
  return (
    <TransactionsContext.Provider value={value}>{children}</TransactionsContext.Provider>
  );
}

function useTransactionsContext() {
  const context = React.useContext(TransactionsContext);
  if (context === undefined) {
    throw new Error("useCounterContext must be used within a CounterProvider");
  }
  return context;
}

export { TransactionsProvider, useTransactionsContext };
