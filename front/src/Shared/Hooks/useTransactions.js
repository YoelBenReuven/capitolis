import { useState, useCallback, useRef, useEffect } from 'react';

export const useTransactions = () => {

  const activeHttpRequests = useRef([]);
  const [transactionsUpdated, setTransactionsUpdated] = useState(false);

  useEffect(() => {
    return () => {
      activeHttpRequests.current.forEach(abortCtrl => abortCtrl.abort());
    };
  }, []);


  const compressTransactions = () => {
    let dic = {};
    const getTransactions =
      async () => {
        try {

          const responseData = await sendRequest(
            `http://localhost:5000/getTransactions`
          );
          if (responseData) {
            for (const transaction of responseData) {
              dic[transaction.counterParty] = dic[transaction.counterParty] ? dic[transaction.counterParty] + Number(transaction.amount) : Number(transaction.amount);
            }
            let arr = [];
            let keys = Object.keys(dic);
            for (const key of keys) {
              let inner = [];
              inner.push(key, dic[key]);
              arr.push(inner);

            }
            const arrr = arr.map(e => e.join(","))
              .join("\n");
            var a = document.createElement('a');
            a.href = 'data:attachment/csv,' + encodeURIComponent(arrr);
            a.target = '_blank';
            a.download = 'transactions.csv';

            document.body.appendChild(a);
            a.click();
          }


        } catch (err) { }
      };
    getTransactions();

  }

  const sendRequest = useCallback(

    async (url, method = 'GET', body = null, headers = {},) => {
      const httpAbortCtrl = new AbortController();
      activeHttpRequests.current.push(httpAbortCtrl);


      try {
        const response = await fetch(url, {
          method,
          body,
          headers,
          signal: httpAbortCtrl.signal
        });
        const responseData = response.json();

        activeHttpRequests.current = activeHttpRequests.current.filter(

          reqCtrl => reqCtrl !== httpAbortCtrl
        );

        if (!response.ok) {

          throw new Error(responseData.message);
        }


        return responseData;
      } catch (err) {

        throw err;
      }
    },
    []
  );

  return { sendRequest, compressTransactions, setTransactionsUpdated, transactionsUpdated };
};

