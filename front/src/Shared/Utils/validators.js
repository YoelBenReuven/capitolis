const VALIDATOR_TYPE_REQUIRE = 'REQUIRE';

export const VALIDATOR_REQUIRE = () => ({ type: VALIDATOR_TYPE_REQUIRE });

export const validate = (value, validators) => {
  let isValid = true;
  for (const validator of validators) {
    if (validator.type === VALIDATOR_TYPE_REQUIRE ) {
      if( typeof value =='string')
      isValid = isValid && value.trim().length > 0;
      else if(value)
      isValid = isValid

    }

    
  }
  return isValid;
};
