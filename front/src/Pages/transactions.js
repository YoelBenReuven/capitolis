import React, { useState } from 'react';
import styled from "styled-components";

import { TransactionsProvider } from '../Shared/Context/useTransactionsContext';
import { useTransactions } from '../Shared/Hooks/useTransactions';
import PayingTranscations from '../Components/LogicComponents/payingTransactions';
import RecievingTransactions from '../Components/LogicComponents/receivingTransactions';
import CButton from '../Components/UI/button';
import Modal from '../Components/UI/modal';



const Transactions = (props) => {
  const { compressTransactions,sendRequest,setTransactionsUpdated,transactionsUpdated} = useTransactions();
  const [isModalOpen,SetModal] =useState(false);

  const toggleModal=()=>{
    SetModal(!isModalOpen);
  }


  return (
    <React.Fragment>
      <TransactionsProvider value={{transactionsUpdated,setTransactionsUpdated,sendRequest,compressTransactions}} >
        <StyledCont>
          <PayingTranscations  style={divStyle}></PayingTranscations>
          <RecievingTransactions style={divStyle}></RecievingTransactions>
        </StyledCont>
        <CButton click={toggleModal}>
          Add Transaction
        </CButton>
        <CButton click={compressTransactions} >
          Compress
        </CButton>
        <Modal close={toggleModal} isOpen={isModalOpen} ></Modal>
      </TransactionsProvider>
    </React.Fragment>



  )


}
const divStyle = {
  flex: '1'
};
const StyledCont = styled.div`
  justify-content: space-around;
  display:flex;

  color: black;
  padding: 5px 7px;
  font-size: 20px;
  margin-top: 40px;
}
`;





export default Transactions;




