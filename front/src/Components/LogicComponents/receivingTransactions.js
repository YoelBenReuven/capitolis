import React, { useEffect,useState } from 'react';
import styled from "styled-components";

import { useTransactionsContext } from '../../Shared/Context/useTransactionsContext';



function RecievingTransactions(){
    const { sendRequest,transactionsUpdated} = useTransactionsContext();
    const [transactions, setTransactions] = useState([]);

    useEffect(() => {
        const getTransactions =
            async () => {
                try {
                    const responseData = await sendRequest(
                        `http://localhost:5000/getTransactions`
                    );              
                    let receiving = responseData.filter(item => {
                        return item.amount > 0;
                    });
                    setTransactions(receiving);
                } catch (err) { }
            };
        getTransactions();

    }, [transactionsUpdated]);


    let html= transactions.map((item,idx)=>{
        
        return(
        <StyledCount key={idx}> 
            <div><span>{idx+1}. </span>{item.counterParty}</div>
            <div>{item.amount}</div>
        </StyledCount>
        )
    })
 
    return (
        <div style={divStyle}>
            <div style={{textAlign:'center'}}>Recieving Transactions</div>
            <div style={{display:'flex',justifyContent:'space-around'}}>
                <span>Counter Party name</span>
                <span>Amount</span >
            </div>
            <div>{html}</div>
        </div>
    )

}
const StyledCount = styled.div`
justify-content: space-around;
display:flex;
flex-direction:row;
color: black;
padding: 5px 7px;
font-size: 20px;
margin-top: 40px;
}
`;
const divStyle = {
    flex:'1'
  };
export default RecievingTransactions;