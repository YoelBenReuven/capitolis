import React, { useState, useReducer } from 'react';
import styled from "styled-components";

import Input from '../UI/input';
import Button from '../UI/button';
import {
    VALIDATOR_REQUIRE,
    validate,
} from '../../Shared/Utils/validators';
import { useTransactionsContext } from '../../Shared/Context/useTransactionsContext';




const NAME_CHANGED = "NAME_CHANGED";
const SUM_CHANGED = "SUM CHANGED";

const reducer = (state, action) => {
    switch (action.type) {
        case NAME_CHANGED:
            return {
                ...state,
                name: { value: action.payload, isValid: validate((action.payload), [VALIDATOR_REQUIRE()]) },
                formIsValid: (() => {
                    return (state.transactionSum.value && state.name.value) != "";
                })()
            }
        case SUM_CHANGED:
            return {
                ...state,
                transactionSum: {
                    value: +action.payload, isValid: validate(+action.payload, [VALIDATOR_REQUIRE()]),

                },
                formIsValid: (() => {
                    return (state.transactionSum.value && state.name.value) != "";
                })()
            }
    }
}


const initState = { formIsValid: false, name: { isValid: false, value: "" }, transactionSum: { isValid: "", value: "" } }

const TransactionForm = () => {

    const { setTransactionsUpdated, sendRequest, } = useTransactionsContext();

    const [state, dispatch] = useReducer(reducer, initState);
    const addTransactionHandler = async (e) => {

        e.preventDefault();

        try {
            let obj = `{ "tradingparty": "Joel", "counterParty": "${state.name.value}","amount":" ${state.transactionSum.value}" }`;
            const responseData = await sendRequest(
                `http://localhost:5000/addTransactions`,
                'POST',
                obj,
                { 'Content-Type': 'application/json' }
            );
            if(responseData){
                setTransactionsUpdated((status) => !status);
            }
        } catch (err) { }
    };
    return (
        <React.Fragment>
            <StyledForm onSubmit={addTransactionHandler}>
                <Input setSum={(e) => dispatch({ type: NAME_CHANGED, payload: e.target.value })} text="Counter Name" ></Input>
                <Input setSum={(e) => dispatch({ type: SUM_CHANGED, payload: e.target.value })} text="Insert Sum for Transaction"></Input>
                <StyledButton disabled={!state.formIsValid}>Commit Transaction</StyledButton>
            </StyledForm>
        </React.Fragment>
    );
};
const StyledButton = styled.button`
    background-color: ${({disabled}) => (disabled ? "red" : "blue")};
    border-radius:5px;
    height:30px;
    margin-top:10px;
    color:white;
   
`;
const StyledForm = styled.form`
margin: auto;
display: flex;
flex-direction: column;
`;

export default TransactionForm;