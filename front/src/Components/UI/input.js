import React from 'react';
import { Button,Input } from '@material-ui/core';

import Label from './label';


const CInput = (props) => {    
    return <div style={{ justifyContent: 'center', display: 'flex' }}>
        <Input onChange={props.setSum} placeholder={props.text} >
            <Label>{props.children}</Label>
        </Input >
    </div>
}
export default CInput;
