import React from 'react';
import { Button } from '@material-ui/core';

import Label from './label';


const CButton = (props) => {    
   
    return (  <div style={{ justifyContent: 'center', display: 'flex' }}>
     <Button type="submit" disabled={props.disabled} onClick={props.click}>
            <Label>{props.children}</Label>
        </Button >
    </div>)
}


export default CButton;
