import React from "react";
import styled from "styled-components";

function Label({children }) {
  return <StyledLabel>{children} </StyledLabel>;
}

const StyledLabel = styled.span`
  background-color: #e9ecef;
  color: #495057;
  padding: 5px 7px;
  border-radius:5px;
`;

export default Label;

